# encoding: utf-8

namespace :coverage do
  ENV["COVERAGE"] = true.to_s
  desc "Generate Code Coverage"
  task :report  => :spec do
    system("rspec spec/")
  end
end

