# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  lastname   :string(255)
#  city       :string(255)
#  phone      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Contact < ActiveRecord::Base
   attr_accessible :name, :lastname, :city, :phone

   validates :name , :presence => true
   validates :lastname , :presence => true
   validates :city , :presence => true
   validates :phone , :presence => true

end
