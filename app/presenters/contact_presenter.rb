class ContactPresenter < Presenter
  def initialize(contact)
    super(
      :id => contact.id,
      :name => contact.name,
      :lastname => contact.lastname,
      :city => contact.city,
      :phone => contact.phone
      ) unless contact.nil?
  end

end
