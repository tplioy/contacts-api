class ContactsPresenter < Presenter
  def initialize(contacts)
    super(
      :contacts => apply_presenter(contacts)
      )
  end

  private
  def apply_presenter(contacts)
    contacts.collect {|c| ContactPresenter.new(c)}
  end

end
