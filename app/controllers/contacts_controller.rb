class ContactsController < ApplicationController


  def index
    @contacts = Contact.all
    render :json => ContactsPresenter.new(@contacts).to_json , :status => 200
  end

  def show
    @contact = Contact.find_by_id(params[:id])
    if @contact.nil?
      render :json => "", :status => 404
    else
      render :json => ContactPresenter.new(@contact).to_json, :status => 200
    end
  end

  def destroy
    @contact = Contact.find_by_id(params[:id])
    if @contact.nil?
      render :json => true, :status => 404
    else
      @contact.delete
      render :json => true, :status => 200
    end
  end

  def create
    @contact = Contact.new(params[:contact])
    if @contact.valid?
      @contact.save
      render :json => @contact, :status => 200
    else
      render :json => @contact, :status => 400
    end
  end

end
