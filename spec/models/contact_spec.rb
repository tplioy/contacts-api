# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  lastname   :string(255)
#  city       :string(255)
#  phone      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe Contact do

  it "has a valid factory" do
    FactoryGirl.create(:contact).should be_valid
  end
  it "is invalid without a name" do
    FactoryGirl.build(:contact, :name => nil).should_not be_valid
  end
  it "is invalid without a lastname" do
    FactoryGirl.build(:contact, :lastname => nil).should_not be_valid
  end
  it "is invalid without a city" do
    FactoryGirl.build(:contact, :city => nil).should_not be_valid
  end
  it "is invalid without a phone" do
    FactoryGirl.build(:contact, :phone => nil).should_not be_valid
  end
end
