require 'spec_helper'

describe ContactsController do

  describe "GET index" do
    before(:each) do
      @contacts = [FactoryGirl.create(:contact),FactoryGirl.create(:contact)]
    end
    it "should list all the contacts" do
      get :index
      assigns(:contacts).should == @contacts
    end
    it "should return 200" do
      get :index
      response.code.should == "200"
    end
  end

  describe "POST create" do
    context "valid attributes" do
      it "should create the contact" do
        expect{
          post :create, :contact => FactoryGirl.attributes_for(:contact)
        }.to change(Contact,:count).by(1)
      end
      it "should return 200" do
        post :create, :contact => FactoryGirl.attributes_for(:contact)
        response.code.should == "200"
      end
    end
    context "invalid attributes" do
      it "should not create the contact" do
        expect{
          post :create, :contact => FactoryGirl.attributes_for(:contact, :name => nil, :lastname => nil, :city => nil, :phone => nil)
        }.to change(Contact,:count).by(0)
      end
      it "should return 400" do
        post :create, :contact => FactoryGirl.attributes_for(:contact, :name => nil, :lastname => nil, :city => nil, :phone => nil)
        response.code.should == "400"
      end
    end
  end

  describe "GET show" do
    before(:each) do
      @contact = FactoryGirl.create(:contact)
    end
    context "valid contact" do
      it "should return the contact" do
        get :show, :id => @contact.id
        assigns(:contact).should == @contact
      end
      it "should return 200" do
        get :show, :id => @contact.id
        response.code.should == "200"
      end
    end
    context "invalid contact" do
      it "should return 404" do
        get :show, :id => "invalid_contact_id"
        response.code.should == "404"
      end
    end
  end


  describe "DELETE destroy" do
    before(:each) do
      @contact = FactoryGirl.create(:contact)
    end
    context "valid contact" do
      it "should destroy the contact" do
        expect{
          delete :destroy, :id => @contact.id
        }.to change(Contact,:count).by(-1)

      end
      it "should return 200" do
        delete :destroy, :id => @contact.id
        response.code.should == "200"
      end
    end
    context "invalid contact" do
      it "should return 404" do
        delete :destroy, :id => "invalid_contact_id"
        response.code.should == "404"
      end
    end
  end
end
