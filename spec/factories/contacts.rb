# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  lastname   :string(255)
#  city       :string(255)
#  phone      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact do |c|
    c.name {Faker::Name.first_name}
    c.lastname {Faker::Name.last_name}
    c.city {Faker::Address.city}
    c.phone {Faker::PhoneNumber.phone_number}
  end
end

